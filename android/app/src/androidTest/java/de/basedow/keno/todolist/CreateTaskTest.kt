package de.basedow.keno.todolist

import androidx.test.espresso.Espresso.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class CreateTaskTest {

    // open To-Do-List app
    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun oneTask() {
        // verify that list is empty
        // click button Create Task
        onView(withId(R.id.create_task)).perform(click())
        // Enter description "Eine Kaffeekanne kaufen"
        onView(withId(R.id.description)).perform(typeText("Eine Kaffeekanne kaufen"))
        // Apply with OK
        // Verify that list contains "Eine Kaffeekanne kaufen"
    }
}
